USE [Flights]
GO
/****** Object:  Table [dbo].[Countries]    Script Date: 12/27/2020 4:15:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Countries](
	[code_Al] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[code_Al] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Flights]    Script Date: 12/27/2020 4:15:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Flights](
	[flight_id] [bigint] IDENTITY(1,1) NOT NULL,
	[timestamp] [datetime] NULL,
	[remaining_seats] [int] NULL,
	[origin_country_id] [bigint] NOT NULL,
	[dest_country_id] [bigint] NOT NULL,
 CONSTRAINT [PK_Flights] PRIMARY KEY CLUSTERED 
(
	[flight_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tickets]    Script Date: 12/27/2020 4:15:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tickets](
	[ticket_id] [bigint] IDENTITY(1,1) NOT NULL,
	[user_id] [bigint] NOT NULL,
	[flight_id] [bigint] NOT NULL,
 CONSTRAINT [PK_Tickets] PRIMARY KEY CLUSTERED 
(
	[ticket_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[users]    Script Date: 12/27/2020 4:15:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[id_Al] [bigint] IDENTITY(1,1) NOT NULL,
	[full_name] [varchar](50) NOT NULL,
	[password] [int] NOT NULL,
	[real_id] [int] NOT NULL,
	[email] [varchar](50) NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[id_Al] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Countries] ON 

INSERT [dbo].[Countries] ([code_Al], [name]) VALUES (3, N'argentina  ')
INSERT [dbo].[Countries] ([code_Al], [name]) VALUES (4, N'USA')
INSERT [dbo].[Countries] ([code_Al], [name]) VALUES (6, N'France')
INSERT [dbo].[Countries] ([code_Al], [name]) VALUES (7, N'Spain')
SET IDENTITY_INSERT [dbo].[Countries] OFF
GO
SET IDENTITY_INSERT [dbo].[Flights] ON 

INSERT [dbo].[Flights] ([flight_id], [timestamp], [remaining_seats], [origin_country_id], [dest_country_id]) VALUES (4, CAST(N'2011-04-12T00:00:00.000' AS DateTime), 3, 3, 6)
INSERT [dbo].[Flights] ([flight_id], [timestamp], [remaining_seats], [origin_country_id], [dest_country_id]) VALUES (6, CAST(N'2013-03-12T00:00:00.000' AS DateTime), 4, 3, 7)
INSERT [dbo].[Flights] ([flight_id], [timestamp], [remaining_seats], [origin_country_id], [dest_country_id]) VALUES (11, CAST(N'2011-03-12T00:00:00.000' AS DateTime), 4, 3, 6)
INSERT [dbo].[Flights] ([flight_id], [timestamp], [remaining_seats], [origin_country_id], [dest_country_id]) VALUES (12, CAST(N'2011-03-12T00:00:00.000' AS DateTime), 4, 3, 4)
INSERT [dbo].[Flights] ([flight_id], [timestamp], [remaining_seats], [origin_country_id], [dest_country_id]) VALUES (14, CAST(N'2011-03-12T00:00:00.000' AS DateTime), 4, 3, 6)
INSERT [dbo].[Flights] ([flight_id], [timestamp], [remaining_seats], [origin_country_id], [dest_country_id]) VALUES (15, CAST(N'2011-03-12T00:00:00.000' AS DateTime), 4, 3, 6)
INSERT [dbo].[Flights] ([flight_id], [timestamp], [remaining_seats], [origin_country_id], [dest_country_id]) VALUES (16, CAST(N'2011-03-12T00:00:00.000' AS DateTime), 4, 3, 6)
SET IDENTITY_INSERT [dbo].[Flights] OFF
GO
SET IDENTITY_INSERT [dbo].[Tickets] ON 

INSERT [dbo].[Tickets] ([ticket_id], [user_id], [flight_id]) VALUES (10, 6, 16)
INSERT [dbo].[Tickets] ([ticket_id], [user_id], [flight_id]) VALUES (14, 6, 11)
SET IDENTITY_INSERT [dbo].[Tickets] OFF
GO
SET IDENTITY_INSERT [dbo].[users] ON 

INSERT [dbo].[users] ([id_Al], [full_name], [password], [real_id], [email]) VALUES (5, N'yosi', 1111, 957471111, NULL)
INSERT [dbo].[users] ([id_Al], [full_name], [password], [real_id], [email]) VALUES (6, N'liel', 1125, 208598459, NULL)
INSERT [dbo].[users] ([id_Al], [full_name], [password], [real_id], [email]) VALUES (7, N'lia', 1112, 157471112, NULL)
INSERT [dbo].[users] ([id_Al], [full_name], [password], [real_id], [email]) VALUES (9, N'meni', 5555, 209437459, NULL)
INSERT [dbo].[users] ([id_Al], [full_name], [password], [real_id], [email]) VALUES (10011, N'dsd', 12234, 123443214, NULL)
INSERT [dbo].[users] ([id_Al], [full_name], [password], [real_id], [email]) VALUES (10012, N'yaaaa', 1234, 12345667, N'yaniv@gmail.com')
INSERT [dbo].[users] ([id_Al], [full_name], [password], [real_id], [email]) VALUES (10013, N'yanivyyy', 1234, 345678345, N'itzhak.yaniv98@gmail.com')
INSERT [dbo].[users] ([id_Al], [full_name], [password], [real_id], [email]) VALUES (10014, N'ssfds', 12, 2312, N'itzhak.yaniv98@gmail.com')
INSERT [dbo].[users] ([id_Al], [full_name], [password], [real_id], [email]) VALUES (10015, N'sfsdf', 23, 234, N'itzhak.yaniv98@gmail.com')
INSERT [dbo].[users] ([id_Al], [full_name], [password], [real_id], [email]) VALUES (10016, N'dfdf', 2, 234342, N'itzhak.yaniv98@gmail.com')
INSERT [dbo].[users] ([id_Al], [full_name], [password], [real_id], [email]) VALUES (10017, N'yosi', 2343, 234324, N'itzhak.yaniv98@gmail.com')
INSERT [dbo].[users] ([id_Al], [full_name], [password], [real_id], [email]) VALUES (10018, N'lia_itzhak', 1234, 345676, N'itzhakliay@gmail.com')
INSERT [dbo].[users] ([id_Al], [full_name], [password], [real_id], [email]) VALUES (20011, N'yaniv1', 111, 111, NULL)
SET IDENTITY_INSERT [dbo].[users] OFF
GO
ALTER TABLE [dbo].[Flights]  WITH CHECK ADD  CONSTRAINT [FK_Flights_countries] FOREIGN KEY([dest_country_id])
REFERENCES [dbo].[Countries] ([code_Al])
GO
ALTER TABLE [dbo].[Flights] CHECK CONSTRAINT [FK_Flights_countries]
GO
ALTER TABLE [dbo].[Flights]  WITH CHECK ADD  CONSTRAINT [FK_Flights_origin_Countries1] FOREIGN KEY([origin_country_id])
REFERENCES [dbo].[Countries] ([code_Al])
GO
ALTER TABLE [dbo].[Flights] CHECK CONSTRAINT [FK_Flights_origin_Countries1]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_Tickets_Flights] FOREIGN KEY([flight_id])
REFERENCES [dbo].[Flights] ([flight_id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_Tickets_Flights]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_Tickets_users] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id_Al])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_Tickets_users]
GO
