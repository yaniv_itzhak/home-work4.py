import os
import utils1.logging_api as logger # requires utils/loggin_api.py
import datetime
import traceback
import pymssql
import json
from flask import Flask
from flask import render_template, request, redirect, url_for

app = Flask(__name__)

def init_logger():
    with open('D:\python_log\Airline.json') as json_file:
        conf = json.load(json_file)
        #log_file_location=D:\temp\logs\ebay.log'\n'
        #log_level=DEBUG
        logger.init(f'{conf["log_file_location"]}'+
                    f'{datetime.datetime.now().year}_'+
                    f'{datetime.datetime.now().month}_' +
                    f'{datetime.datetime.now().day}_' +
                    f'{datetime.datetime.now().hour}_' +
                    f'{datetime.datetime.now().minute}_' +
                    f'{datetime.datetime.now().second}' + '.log'
                    , conf["log_level"])

def test_db_connection():
    try:
        logger.write_lo_log(f'Testing connection to [{conf["server"]}] [{conf["database"]}]', 'INFO')
        conn = pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database'])
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to connecto to db [{conf["server"]}] [{conf["database"]}]', 'ERROR')
        logger.write_lo_log(f'Failed to connecto to db {e}', 'ERROR')
        logger.write_lo_log(f'Failed to connecto to db {tr}', 'ERROR')
        print('Faild to connect to db ... exit')
        exit(-1)

@app.route('/users', methods = ['GET'])
def get_all_users():
    logger.write_lo_log('**************** methods = GET ...', 'INFO')
    
    print(request.method)
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            conn.execute_query('SELECT * FROM users')
            result = []
            for row in conn:
                print(f'{row["full_name"]} ')
                result.append(f'id : {row["id_Al"]}')
                result.append({'full name': row["full_name"]})
                result.append({'password': row["password"]})
                result.append({'real id': row["real_id"]})

            print('=================== was pymssql._mssql connector')
            print(result)
            return json.dumps(result)
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [SELECT * FROM users] to db {tr}', 'ERROR')
        return json.dumps({'Error' : e})




@app.route('/users/<int:id>', methods = ['GET'])
def get_by_id(id):
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            query = f'SELECT * FROM users WHERE id_Al={id}'
            conn.execute_query(query)
            result = []
            for row in conn:
                print(f'{row["id_Al"]} {row["full_name"]} {row["password"]} {row["real_id"]}')
                # SALARY requires exytra parsing
                result = {'ID': row["id_Al"], 'NAME': row["full_name"], 'password': row["password"] ,'real_id':row["real_id"]}
            print(result)
            return json.dumps(result)
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error': e})





@app.route('/users/<int:id>', methods = ['PUT'])
def put_by_id(id):
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            updated_users = request.get_json()
            query = f"UPDATE users SET full_name=('{updated_users['full_name']}'),password=('{updated_users['password']}') ,real_id=('{updated_users['real_id']}')"+\
             f"WHERE id_Al={id} "
            conn.execute_query(query)
            return json.dumps({'result' : 'succeed'})
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error': e})


@app.route('/users', methods = ['POST'])
def post_new_users():
    logger.write_lo_log('/users POST', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            new_users = request.get_json()
            print(new_users)
            logger.write_lo_log(f'/customers POST new customer {new_users}', 'INFO')
            query = 'INSERT INTO users (full_name, password, real_id)' + \
                    f"VALUES ('{new_users['full_name']}','{new_users['password']}', '{new_users['real_id']}');"
            logger.write_lo_log(f'/customers POST new customer query {query}', 'DEBUG')
            conn.execute_query(query)
            return json.dumps({'result': 'succeed'})
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error': e})






@app.route('/users/<int:id>', methods = ['DELETE'])
def delete_users(id):
    logger.write_lo_log('/users POST', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            delete_user = request.get_json()
            print(delete_user)
            logger.write_lo_log(f'/customers POST new customer {delete_user}', 'INFO')
            query = f'DELETE  FROM users WHERE id_Al={id}'
            logger.write_lo_log(f'/customers POST new customer query {query}', 'DEBUG')
            conn.execute_query(query)
            return json.dumps({'result': 'succeed'})
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error': e})








def main():
    init_logger()
    logger.write_lo_log('**************** System started ...', 'INFO')

    #test_db_connection()

with open('D:\python_log\Airline.json') as json_file:
    conf = json.load(json_file)



main()


app.run(debug=True)
