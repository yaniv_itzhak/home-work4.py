import os
import utils1.logging_api as logger # requires utils/loggin_api.py
import datetime
import traceback
import pymssql
import json
from flask import Flask
from flask import render_template, request, redirect, url_for

app = Flask(__name__)

def init_logger():
    with open('D:\python_log\Airline.json') as json_file:
        conf = json.load(json_file)
        #log_file_location=D:\temp\logs\ebay.log'\n'
        #log_level=DEBUG
        logger.init(f'{conf["log_file_location"]}'+
                    f'{datetime.datetime.now().year}_'+
                    f'{datetime.datetime.now().month}_' +
                    f'{datetime.datetime.now().day}_' +
                    f'{datetime.datetime.now().hour}_' +
                    f'{datetime.datetime.now().minute}_' +
                    f'{datetime.datetime.now().second}' + '.log'
                    , conf["log_level"])

def test_db_connection():
    try:
        logger.write_lo_log(f'Testing connection to [{conf["server"]}] [{conf["database"]}]', 'INFO')
        conn = pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database'])
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to connecto to db [{conf["server"]}] [{conf["database"]}]', 'ERROR')
        logger.write_lo_log(f'Failed to connecto to db {e}', 'ERROR')
        logger.write_lo_log(f'Failed to connecto to db {tr}', 'ERROR')
        print('Faild to connect to db ... exit')
        exit(-1)


######################users
@app.route('/users', methods = ['GET'])
def get_all_users():
    logger.write_lo_log('**************** methods = GET ...', 'INFO')
    query = ''
    print(request.method)
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            query='SELECT * FROM users'
            conn.execute_query(query)
            logger.write_lo_log(f'/GET users {query}','INFO')
            result = []
            for row in conn:
                print(f'{row["full_name"]} ')
                result.append(f'id : {row["id_Al"]}')
                result.append({'full name': row["full_name"]})
                result.append({'password': row["password"]})
                result.append({'real id': row["real_id"]})

            print('=================== was pymssql._mssql connector')
            print(result)
            return json.dumps(result)
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run {query} to db {tr}', 'ERROR')
        return json.dumps({'Error' : e})




@app.route('/users/<int:id>', methods = ['GET'])
def get_by_id(id):
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            logger.write_lo_log(f'/GET users/{id}','INFO')
            query = f'SELECT * FROM users WHERE id_Al={id}'
            conn.execute_query(query)
            logger.write_lo_log(f'GET users/{id} {query}' ,'DEBUG')
            result = []
            for row in conn:
                print(f'{row["id_Al"]} {row["full_name"]} {row["password"]} {row["real_id"]}')
                result = {'ID': row["id_Al"], 'NAME': row["full_name"], 'password': row["password"] ,'real_id':row["real_id"]}
            print(result)
            return json.dumps(result)
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error': e})





@app.route('/users/<int:id>', methods = ['PUT'])
def put_by_id(id):
    logger.write_lo_log('/users PUT', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            updated_users = request.get_json()
            query = f"UPDATE users SET full_name=('{updated_users['full_name']}'),password=('{updated_users['password']}') ,real_id=('{updated_users['real_id']}')"+\
             f"WHERE id_Al={id} "
            logger.write_lo_log(f'/users PUT new customer query {query}', 'DEBUG')
            conn.execute_query(query)
            return json.dumps({'result' : 'succeed'})
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error': e})


@app.route('/users', methods = ['POST'])
def post_new_users():
    logger.write_lo_log('/users POST', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            new_users = request.get_json()
            print(new_users)
            logger.write_lo_log(f'/customers POST new customer {new_users}', 'INFO')
            query = 'INSERT INTO users (full_name, password, real_id)' + \
                    f"VALUES ('{new_users['full_name']}','{new_users['password']}', '{new_users['real_id']}');"
            logger.write_lo_log(f'/users POST new user query {query}', 'DEBUG')
            conn.execute_query(query)
            return json.dumps({'result': 'succeed'})
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error': e})



@app.route('/users/<int:id>', methods = ['DELETE'])
def delete_users(id):
    logger.write_lo_log('/users POST', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            delete_user = request.get_json()
            print(delete_user)
            logger.write_lo_log(f'/customers DELETE new customer {delete_user}', 'INFO')
            query = f'DELETE  FROM users WHERE id_Al={id}'
            logger.write_lo_log(f'/users DELETE user query {query}', 'DEBUG')
            conn.execute_query(query)
            return json.dumps({'result': 'succeed'})
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error': e})

######################## Countries
@app.route('/Countries', methods = ['GET'])
def get_all_Countries():
    logger.write_lo_log('**************** methods = GET ...', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            query='SELECT * FROM Countries'
            conn.execute_query(query)
            logger.write_lo_log(f'/GET Countries {query}','INFO')
            result = []
            for row in conn:
                print(f'{row["code_Al"]} : {row["name"]} ')
                result.append(f'code_Al : {row["code_Al"]}')
                result.append({'name': row["name"]})
            print('=================== was pymssql._mssql connector')
            print(result)
            logger.write_lo_log(f'/GET Countries {result}', 'INFO')
            return json.dumps(result)
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run {query} to db {tr}', 'ERROR')
        return json.dumps({'Error' : e})

@app.route('/Countries/<int:id>', methods = ['GET'])
def get_by_Countries(id):
    logger.write_lo_log('**************** methods = GET ...', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            query=f'SELECT * FROM Countries WHERE code_Al={id} '
            conn.execute_query(query)
            logger.write_lo_log(f'/GET Countries {query}','INFO')
            result = []
            for row in conn:
                print(f'{row["code_Al"]} : {row["name"]} ')
                result.append(f'code_Al : {row["code_Al"]}')
                result.append({'name': row["name"]})
            print('=================== was pymssql._mssql connector')
            print(result)
            logger.write_lo_log(f'/GET Countries {result}', 'INFO')
            return json.dumps(result)
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run {query} to db {tr}', 'ERROR')
        return json.dumps({'Error' : e})

@app.route('/Countries/<int:id>', methods = ['PUT'])
def put_Countries_by_id(id):
    logger.write_lo_log('/Countries PUT', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            updated_Countries = request.get_json()
            query = f"UPDATE Countries SET name=('{updated_Countries['name']}')WHERE code_Al={id}"
            logger.write_lo_log(f'/Countries PUT Countries  query {query}', 'DEBUG')
            conn.execute_query(query)
            return json.dumps({'result' : 'succeed'})
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error': e})

@app.route('/Countries', methods = ['POST'])
def POST_Countries():
    logger.write_lo_log('/Countries POST', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            new_Countries = request.get_json()
            print(new_Countries)
            logger.write_lo_log(f'/Countries POST new customer {new_Countries}', 'INFO')
            query = 'INSERT INTO Countries (name)' + \
                    f"VALUES ('{new_Countries['name']}');"
            logger.write_lo_log(f'/Countries POST new user query {query}', 'DEBUG')
            conn.execute_query(query)
            return json.dumps({'result': 'succeed'})
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error': e})

@app.route('/Countries/<int:id>', methods = ['DELETE'])
def delete_Countries(id):
    logger.write_lo_log('/Countries DELETE', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            delete_country = request.get_json()
            print(delete_country)
            logger.write_lo_log(f'/Countries DELETE  country {delete_country}', 'INFO')
            query = f"DELETE FROM Countries WHERE code_Al={id};"
            logger.write_lo_log(f'/Countries DELETE country query {query}', 'DEBUG')
            conn.execute_query(query)
            return json.dumps({'result': 'succeed'})
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error': e})

####################Flights
@app.route('/Flights', methods = ['GET'])
def get_all_Flights():
    logger.write_lo_log('**************** methods = GET ...', 'INFO')
    query = ''
    print(request.method)
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            query='SELECT * FROM Flights'
            conn.execute_query(query)
            logger.write_lo_log(f'/GET Flights {query}','INFO')
            result = []
            for row in conn:
                print(f'{row["flight_id"]} ')
                result.append(f'timestamp : {row["timestamp"]}')
                result.append({'remaining_seats': row["remaining_seats"]})
                result.append({'origin_country_id': row["origin_country_id"]})
                result.append({'dest_country_id': row["dest_country_id"]})

            print('=================== was pymssql._mssql connector')
            print(result)
            return json.dumps(result)
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run {query} to db {tr}', 'ERROR')
        return json.dumps({'Error' : e})

@app.route('/Flights/<int:id>', methods = ['GET'])
def get_by_Flights(id):
    logger.write_lo_log('**************** methods = GET ...', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            query=f'SELECT * FROM Flights WHERE flight_id={id} '
            conn.execute_query(query)
            logger.write_lo_log(f'/GET Countries {query}','INFO')
            result = []
            for row in conn:
                print(f'{row["flight_id"]} : {row["flight_id"]} ')
                result.append(f'flight_id : {row["flight_id"]}')
                result.append(f'timestamp : {row["timestamp"]}')
                result.append(f'remaining_seats : {row["remaining_seats"]}')
                result.append(f'origin_country_id : {row["origin_country_id"]}')
                result.append(f'dest_country_id : {row["dest_country_id"]}')
            print('=================== was pymssql._mssql connector')
            print(result)
            logger.write_lo_log(f'/GET Flights {result}', 'INFO')
            return json.dumps(result)
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run {query} to db {tr}', 'ERROR')
        return json.dumps({'Error' : e})


@app.route('/Flights/<int:id>', methods = ['PUT'])
def put_Flights_by_id(id):
    logger.write_lo_log('/Flights PUT', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            updated_Flights = request.get_json()
            query = f"UPDATE Flights SET timestamp=('{updated_Flights['timestamp']}'),remaining_seats=('{updated_Flights['remaining_seats']}'),origin_country_id=('{updated_Flights['origin_country_id']}'),dest_country_id=('{updated_Flights['dest_country_id']}') WHERE flight_id={id}"
            logger.write_lo_log(f'/Countries PUT Countries  query {query}', 'DEBUG')
            conn.execute_query(query)
            return json.dumps({'result' : 'succeed'})
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error': e})

@app.route('/Flights', methods = ['POST'])
def POST_Flights():
    logger.write_lo_log('/Flights POST', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            new_Flights = request.get_json()
            print(new_Flights)
            logger.write_lo_log(f'/Flights POST new_Flights {new_Flights}', 'INFO')
            query = 'INSERT INTO Flights (timestamp,remaining_seats,origin_country_id,dest_country_id)' + \
                    f"VALUES ('{new_Flights['timestamp']}','{new_Flights['remaining_seats']}','{new_Flights['origin_country_id']}','{new_Flights['dest_country_id']}') ;"
            logger.write_lo_log(f'/Flights POST new Flights query {query}', 'DEBUG')
            conn.execute_query(query)
            return json.dumps({'result': 'succeed'})
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error': e})

@app.route('/Flights/<int:id>', methods = ['DELETE'])
def delete_Flights(id):
    logger.write_lo_log('/Flights DELETE', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            delete_Flight = request.get_json()
            print(delete_Flight)
            logger.write_lo_log(f'/Flights DELETE  Flight {delete_Flight}', 'INFO')
            query = f'DELETE  FROM Flights WHERE flight_id={id}'
            logger.write_lo_log(f'/users DELETE user query {query}', 'DEBUG')
            conn.execute_query(query)
            return json.dumps({'result': 'succeed'})
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error': e})


def main():
    init_logger()
    logger.write_lo_log('**************** System started ...', 'INFO')

    #test_db_connection()

with open('D:\python_log\Airline.json') as json_file:
    conf = json.load(json_file)



main()


app.run(debug=True)

