def print_board(board):
    "this  function do Intended Print the board "
    print('0 1 2')

    for i in range(3):
        for j in range(3):
            print(board[i][j], end=" ")
        print()

def get_board():
    board = [
        ["_", "_", "_"],
        ["_", "_", "_"],
        ["_", "_", "_"]
    ]
    return board

def new_board(board):
    "this  function do Intended Print the board "
    print('0 1 2')

    for i in range(3):
        for j in range(3):
            print(board[i][j], end=" ")
        print()

def get_x_player(board):
    """
    this  function do Intended get row and col X player
    :return:
    """
    while True:
        row_x=int(input('Player X please enter row [0-2]:'))
        if row_x < 0 or row_x > 2:
            print('must be between 0-2')
        else:
            break

    while True :
        col_x = int(input('Player X please enter col [0-2]:'))
        if col_x < 0 or col_x > 2:
            print('must be between 0-2')
        else:
            break

    board[row_x][col_x] ='X'
    print_board(board)

def O_player(board):
    """
     this function Intended get row and col O player
     """
    while True:
        row_O=int(input('Player O please enter row [0-2]:'))
        if row_O < 0 or row_O > 2:
            print('must be between 0-2')
        else:
            break

    while True :
        col_O = int(input('Player O please enter col [0-2]:'))
        if col_O < 0 or col_O > 2:
            print('must be between 0-2')
        else:
            break
    board[row_O] [col_O] ='O'

    print_board(board)

def row_ABC(board, sequence_x=('X', 'X', 'X'),sequence_O =('O', 'O', 'O')):
    """
    this  function do Checking the sequence on the board in row zero ,one and two

    :param sequence_x:Checking the sequence
    :param sequence_O:Checking the sequence
    :return:
    """
    row_A = board[0][0], board[0][1], board[0][2]
    if row_A == sequence_x:
        print('player X win!')
    elif row_A == sequence_O:
        print('player O win! ')

    row_B = board[1][0], board[1][1], board[1][2]

    if row_B == sequence_x:
        print('player X win!')

    elif row_B == sequence_O:
        print('player O win! ')

    row_C = board[2][0], board[2][1], board[2][2]

    if row_C == sequence_x:
        print('player X win!')
    elif row_C == sequence_O:
        print('player O win! ')

def col_ABC(board, sequence_x=('X', 'X', 'X'),sequence_O =('O', 'O', 'O')):
    """
     this  function do Checking the sequence on the board in col zero ,one and two

    :param sequence_x: sequence_x=('X', 'X', 'X')
    :param sequence_O: sequence_O =('O', 'O', 'O')):
    :return:
    """

    col_A = board[0][0], board[1][0], board[2][0]

    if col_A == sequence_x:
        print('player X win!')
    elif col_A == sequence_O:
        print('player O win! ')

    col_B = board[0][1], board[1][1], board[2][1]

    if col_B == sequence_x:
        print('player X win!')
    elif col_B == sequence_O :
        print('player O win! ')


    col_C = board[0][2], board[1][2], board[2][2]

    if col_C == sequence_x:
        print('player X win!')
    elif col_C == sequence_O:
        print('player O win! ')

def slant(board, sequence_x=('X', 'X', 'X'),sequence_O =('O', 'O', 'O')):
    """
    this function do Checking the sequence on the board in slant zero and two
    :param sequence_x: sequence_x=('X', 'X', 'X')
    :param sequence_O: sequence_O =('O', 'O', 'O')
    :return:
    """

    slant_A = board[0][0], board[1][1], board[2][2]

    if slant_A == sequence_x:
        print('player X win!')
    elif slant_A == sequence_O:
        print('player O win! ')


    slant_B = board[0][2], board[1][1], board[2][0]

    if slant_B == sequence_x:
        print('player X win!')
    elif slant_B == sequence_O:
        print('player O win! ')

def main_game(coefficient=0 ):
    """
    this  function do run all Functions but Stops if coefficient ==3
    :param coefficient:
    :return:
    """
    board = get_board()
    print_board(board)
    while True:
        coefficient += 1
        #print_board(board)
        get_x_player(board)
        O_player(board)
        row_ABC(board)
        col_ABC(board)
        slant(board)
        if coefficient == 3:
            break


def play_game(new=0):
    """
    this  function do run game.
     That the user -1 entersAnd a new game
    :param new:
    :return:
    """

    while True:
        new = int(input('please enter -1 for start new game '))
        if new != -1 :
            break
        main_game()


play_game()
